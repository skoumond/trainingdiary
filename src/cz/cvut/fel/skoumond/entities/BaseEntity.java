
package cz.cvut.fel.skoumond.entities;

import java.io.Serializable;

/**
 *
 * Abstraktní třída reprezentující položku databáze.
 * 
 * @author Ondra
 */
public abstract class BaseEntity implements Serializable {
       
    /**
     * Identifikační číslo položky tabulky.
     */
    private int id;

    /**
     * Konstruktor položky, inicilaizuje identifikační číslo.
     * 
     * @param id 
     */
    public BaseEntity(int id) {
        this.id = id;
    }

    /**
     * Vrací identifikační číslo objektu.
     * 
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * Vrací hashcode.
     * 
     * @return hashcode
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + this.id;
        return hash;
    }

    /**
     * Nastavuje správné fungování metody Equals.
     * 
     * @param obj
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BaseEntity other = (BaseEntity) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    /**
     * 
     * @return základní výpis
     */
    @Override
    public String toString() {
        return "EntityBase{" + "id=" + id + '}';
    }
       
}
