
package cz.cvut.fel.skoumond.entities;

import cz.cvut.fel.skoumond.utilities.DateReader;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Objects;

/**
 * Třída reprezentující jeden tréninkový záznam v diáři.
 * 
 * @author Ondra
 */
public class RunEntity extends BaseEntity implements Serializable {
    
    /**
     * Datový typ Calendar, sloužící k uložení data a času.
     */
    private Calendar dateTime;
    /**
     * Popis tréninkového záznamu.
     */
    private String desc;
    /**
     * Délka tréninku.
     */
    private int sessionTime;
    /**
     * Vzdálenost uražená při tréninku.
     */
    private float distInKm;
    /**
     * Průměrná tepové frekvence při tréninku.
     */
    private int avrgHeartRate;
    /**
     * Reprezentace typu tréninku.
     */
    private TypeEntity type; 

    /**
     * Konstruktor záznamu, inicilaizuje všechny své hodnoty.
     * 
     * @param datumCas
     * @param desc
     * @param sessionTime
     * @param distInKm
     * @param avrgHeartRate
     */
    public RunEntity(int id, Calendar dateTime, String desc, int sessionTime, float distInKm, int avrgHeartRate, TypeEntity type) {
        super(id);
        this.dateTime = dateTime;
        this.desc = desc;
        this.sessionTime = sessionTime;
        this.distInKm = distInKm;
        this.avrgHeartRate = avrgHeartRate;
        this.type = type;
    }
    
    /**
     * Prázdný konstruktor, slouží pro správnou funkci dalších metod.
     */
    public RunEntity(int id) {
        super(id);
        this.dateTime = Calendar.getInstance();
        this.desc = "desc";
        this.sessionTime = 0;
        this.distInKm = 0;
        this.avrgHeartRate = 0;
        this.type = new TypeEntity(0, "");
    }

    /**
     * Vrací datum a čas.
     * 
     * @return 
     */
    public Calendar getDateTime() {
        return dateTime;
    }

    /**
     * Nastavuje datum a čas.
     * 
     * @param datumCas
     */
    public void setDateTime(Calendar dateTime) {
        this.dateTime = dateTime;
    }

    /**
     * Vrací popis záznamu.
     * 
     * @return
     */
    public String getDesc() {
        return desc;
    }

    /**
     * Nastavuje popis záznamu.
     * 
     * @param desc
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }
    
    /**
     * Vrací délku tréninku.
     * 
     * @return
     */
    public int getSessionTime() {
        return sessionTime;
    }
    
    /**
     * Nastavuje délku tréninku.
     * 
     * @param sessionTime
     */
    public void setSessionTime(int sessionTime) {
        this.sessionTime = sessionTime;
    }
    
    /**
     * Vrací uraženou vzdálenost při tréninku.
     * 
     * @return
     */
    public float getDistInKm() {
        return distInKm;
    }
    
    /**
     * Nastavuje uraženou vzdálenost při tréninku.
     * 
     * @param distInKm
     */
    public void setDistInKm(float distInKm) {
        this.distInKm = distInKm;
    }
    
    /**
     * Vrací průměrnou tepovou frekvenci.
     * 
     * @return avrgHeartRate
     */
    public int getAvrgHeartRate() {
        return avrgHeartRate;
    }
    
    /**
     * Nastavuje průměrnou tepovou frekvenci.
     * 
     * @param avrgHeartRate
     */
    public void setAvrgHeartRate(int avrgHeartRate) {
        this.avrgHeartRate = avrgHeartRate;
    }

    /**
     * Vrací typeEntitu.
     * 
     * @return type
     */
    public TypeEntity getType() {
        return type;
    }

    /**
     * Nastavuje entitu.
     * 
     * @param type 
     */
    public void setType(TypeEntity type) {
        this.type = type;
    }

    /**
     * Nastavuje hashcode.
     * 
     * @return hashcode
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.dateTime);
        hash = 37 * hash + Objects.hashCode(this.desc);
        hash = 37 * hash + this.sessionTime;
        hash = 37 * hash + Float.floatToIntBits(this.distInKm);
        hash = 37 * hash + this.avrgHeartRate;
        hash = 37 * hash + Objects.hashCode(this.type);
        return hash;
    }

    /**
     * 
     * Nastavuje správné fungování metody Equals.
     * 
     * @param obj
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RunEntity other = (RunEntity) obj;
        if (!Objects.equals(this.dateTime, other.dateTime)) {
            return false;
        }
        if (!Objects.equals(this.desc, other.desc)) {
            return false;
        }
        if (this.sessionTime != other.sessionTime) {
            return false;
        }
        if (Float.floatToIntBits(this.distInKm) != Float.floatToIntBits(other.distInKm)) {
            return false;
        }
        if (this.avrgHeartRate != other.avrgHeartRate) {
            return false;
        }
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        return true;
    }
    /**
     * Nastavuje správné fungování výpisu objektu.
     * 
     * @return Vrací formátovaný řetězec objektu RunEntity, formátuje 
     * dílčí atributy, každý atribut má svůj řádek.
     */
    @Override
    public String toString() {
        return "Čas: " + DateReader.formatData.format(dateTime.getTime()) + 
                String.format("%nPopis tréninku: %s%nDélka tréninku: %s min%n"
                + "Uběhnutá vzdálenost: %.1f km%n"
                + "Průměrná tepová frekvence: %s tepů za vteřinu%n", 
                desc, sessionTime, distInKm, avrgHeartRate) + 
                type.toString();
    }
}
