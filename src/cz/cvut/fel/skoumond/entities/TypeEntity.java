
package cz.cvut.fel.skoumond.entities;

import java.io.Serializable;
import java.util.Objects;

/**
 * Třída reprezentující typ tréninků v diáři.
 * 
 * @author Ondra
 */
public class TypeEntity extends BaseEntity implements Serializable {
 
    /**
     * Název typu tréninku.
     */
    private String type;
    
    /**
     * Konstruktor
     * 
     * 
     * @param id
     * @param type 
     */
    public TypeEntity(int id, String type) {
        super(id);
        this.type = type;
    }

    /**
     * Vrací typ tréninku.
     * 
     * @return type
     */
    public String getType() {
        return type;
    }

    /**
     * Zadává typ tréninku.
     * 
     * @param type 
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Nastavuje správný hashcode.
     * 
     * @return hashcode
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Objects.hashCode(this.type);
        return hash;
    }

    /**
     * Nastavuje správné fungování metody Equals.
     * 
     * @param obj
     * @return 
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TypeEntity other = (TypeEntity) obj;
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        return true;
    }

    /**
     * Vrací správný tvar objektu ve formátu String.
     * 
     * @return String
     */
    @Override
    public String toString() {
        return "Type: " + type + "\n\n";
    }
}
