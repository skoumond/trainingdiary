
package cz.cvut.fel.skoumond.gui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Arrays;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author Ondra
 */
class LoginDialog extends JDialog {

    private JTextField useridField;
    private JPasswordField passwordField;
    private boolean notCancelled;
    private Connection connect;
    private JPanel jPanel;
    private JLabel jLabel;
    
    // Needed for dialogs
    private JFrame jFrame; 

    public LoginDialog(JFrame jFrame) {
        super(jFrame, "Loading Training activities...", true);
        
        this.jFrame = jFrame;
        
        initDialogLayout();
        setSize(300, 150);
        setLocationRelativeTo(null);

        notCancelled = false;

        pack();
        setVisible(true);
    }

    public Connection getConnection() {
        return connect;
    }

    public boolean notCancel() {
        return notCancelled;
    }

    private void initDialogLayout() {

        Container pane = getContentPane();
        pane.setLayout(new BorderLayout());

        pane.add(getLine1(), BorderLayout.NORTH);

        pane.add(getLine2(), BorderLayout.CENTER);

        pane.add(getButtonPanel(), BorderLayout.SOUTH);
    }

    private JPanel getLine1() {
        jPanel = new JPanel(new FlowLayout());
        jLabel = new JLabel("Your username:", JLabel.LEFT);
        jPanel.add(jLabel);
        useridField = new JTextField(10);

        jLabel.setLabelFor(useridField);
        jPanel.add(useridField);

        return jPanel;
    }

    private JPanel getLine2() {
        jPanel = new JPanel(new FlowLayout());
        jLabel = new JLabel("Your password:", JLabel.LEFT);
        jPanel.add(jLabel);

        //Create everything.
        passwordField = new JPasswordField(10);

        jLabel.setLabelFor(passwordField);
        jPanel.add(passwordField);

        return jPanel;
    }

    private JPanel getButtonPanel() {      
        JPanel panel = new JPanel();
        JButton jButton = new JButton("Ok");
        
        jButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                String username = useridField.getText();
                char[] password = passwordField.getPassword();
                boolean success = false;
                
                if ((isPasswordCorrect(password)) && (isUsernameCorrect(username))) {
                    success = true;
                } else {
                    JOptionPane.showMessageDialog(jFrame,
                            "Bad password or username. Try it again.",
                            "Error Message",
                            JOptionPane.ERROR_MESSAGE);
                }

                //Zero out the possible password, for security.
                Arrays.fill(password, '0');

                passwordField.selectAll();
                resetFocus();
                
                if (success) {
                    onDialogOk();
                }  
            }
        });
        panel.add(jButton);
        jButton = new JButton("Back");
        jButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                onDialogCancel();
            }
        });
        panel.add(jButton);
        return panel;
    }

    private void onDialogOk() {
        if (attemptConnection()) {
            setVisible(false);
            notCancelled = true;
        }
    }

    private void onDialogCancel() {
        setVisible(false);
    }

    private boolean attemptConnection() {
        try {
            connect = DriverManager.getConnection("jdbc:derby:database;create=true", "skoumond", "heslo");
            return true;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this,
                    "Chyba při připojení k databázi: "
                    + e.getMessage());
        }
        return false;
    }

    /**
     * Checks the passed-in array against the correct password. After this
     * method returns, you should invoke eraseArray on the passed-in array.
     */
    private boolean isPasswordCorrect(char[] input) {
        boolean isCorrect = true;
        char[] correctPassword = {'a','d','m','i','n' };

        if (input.length != correctPassword.length) {
            isCorrect = false;
        } else {
            isCorrect = Arrays.equals(input, correctPassword);
        }

        //Zero out the password.
        Arrays.fill(correctPassword, '0');

        return isCorrect;
    }
    
    private boolean isUsernameCorrect(String input) {
        boolean isCorrect = true;
        String correctString = "Admin";

        if (input.length() != correctString.length()) {
            isCorrect = false;
        } else {
            if (input.compareTo(correctString) != 0) {
                 isCorrect = false;
             }
        }

        return isCorrect;
    }

    //Must be called from the event dispatch thread.
    protected void resetFocus() {
        passwordField.requestFocusInWindow();
    }
}
