package cz.cvut.fel.skoumond.gui;

import cz.cvut.fel.skoumond.entities.RunEntity;
import cz.cvut.fel.skoumond.entities.TypeEntity;
import cz.cvut.fel.skoumond.utilities.FileUtils;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.LookAndFeel;
import javax.swing.RowFilter;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Ondra
 */
public class Mainframe extends JFrame implements ActionListener, ItemListener {

    // Possible Look & Feels
    private static final String mac =
            "com.apple.laf.AquaLookAndFeel";
    private static final String nimbus =
            "javax.swing.plaf.nimbus.NimbusLookAndFeel";
    private static final String metal =
            "javax.swing.plaf.metal.MetalLookAndFeel";
    private static final String motif =
            "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
    private static final String windows =
            "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
    private static final String gtk =
            "com.sun.java.swing.plaf.gtk.GTKLookAndFeel";
    
    // The current Look & Feel
    private static String currentLookAndFeel = nimbus;
   
    // The preferred size of the GUI
    private static final int PREFERRED_WIDTH = 800;
    private static final int PREFERRED_HEIGHT = 450;
    
    // Login dialog 
    private Connection connection = null;
   
    // Manual centering
    private Dimension dimension = null;
   
    // How to filter
    private JTextField filterText = null;
   
    // Search box
    private JComboBox searchBox = null;
   
    // Search labels
    private String[] searchBy = {
        "Search by description",
        "Search by session time",
        "Search by distance",
        "Search by heart rate",
        "Search by activity type"
    };
   
    // Create table sorter
    private TableRowSorter<ResultSetTableModel> sorter;
   
    // Default search is by description
    private int selection = 1;
   
    // Menus
    private JMenuBar jMenuBar = null;
    private JMenu jMenu = null;
    private JMenu submenu = null;
    private JMenuItem jMenuItem = null;
    private JMenuItem jMenuItemAdd = null;
    private JMenuItem jMenuItemEdit = null;
    private JMenuItem jMenuItemRemove = null;
    private JMenuItem jMenuItemRemoveAll = null;
    private ButtonGroup styleMenu = new ButtonGroup();
   
    // Pupup menu
    private JPopupMenu jPopupMenu = null;
   
    // Main table
    private JTable jTable = null;
  
    // Table model
    private ResultSetTableModel resultModel = null;
   
    // Panels and labels 
    private JPanel jPanel = null;
    private JLabel jLabel = null;

    /**
     * Gui Constructor
     */
    public Mainframe() {
        super("Training activity diary");

        initLookAndFeel();

        final LoginDialog dialog = new LoginDialog(this);

        //Make sure the focus goes to the right component
        //whenever the frame is initially given the focus.
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowActivated(WindowEvent e) {
                dialog.resetFocus();
            }
        });

        if (dialog.notCancel()) {
            connection = dialog.getConnection();
            initFrameLayout();
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            //pack();
            setSize(new Dimension(PREFERRED_WIDTH, PREFERRED_HEIGHT));
            setCenterLocation();
            setVisible(true);
        } else {
            dispose();
        }
    }

    private void initFrameLayout() {

        // Container to hold diary
        Container pane = getContentPane();

        // Layout
        pane.setLayout(new BorderLayout());

        // Menu
        initJMenuBar();

        // 1 - Searchbox
        pane.add(getSearchBox(), BorderLayout.NORTH);
        // 2 - Table at center
        pane.add(new JScrollPane(getCentralTable()), BorderLayout.CENTER);
        // 3 - Bottom panel
        pane.add(getBottomPanel(), BorderLayout.SOUTH);
    }

    private JTable getCentralTable() {

        // Table data model
        resultModel = new ResultSetTableModel();
        sorter = new TableRowSorter<>(resultModel);
        jTable = new JTable(resultModel);
        jTable.setRowSorter(sorter);

        //
        jTable.setPreferredScrollableViewportSize(new Dimension(500, 70));
        jTable.setFillsViewportHeight(true);

        // Scroller
        jTable.addComponentListener(new Mainframe.TableScroller());

        // Popup menu for the table
        jTable.setComponentPopupMenu(getPopupMenu());

        // Mouse listener
        jTable.addMouseListener(new TableMouseListener(jTable));

        return jTable;
    }

    private JPanel getBottomPanel() {

        // Holds text filter field and exit button
        jPanel = new JPanel(new BorderLayout());
        jLabel = new JLabel("Search:");
        jPanel.add(jLabel, BorderLayout.NORTH);

        filterText = new JTextField();

        //Whenever filterText changes, invoke newFilter.
        filterText.getDocument().addDocumentListener(
                new DocumentListener() {
            @Override
            public void changedUpdate(DocumentEvent de) {
                filter();
            }

            @Override
            public void insertUpdate(DocumentEvent de) {
                filter();
            }

            @Override
            public void removeUpdate(DocumentEvent de) {
                filter();
            }
        });

        // Place filter
        jLabel.setLabelFor(filterText);
        jPanel.add(filterText, BorderLayout.CENTER);

        // Add exit button
        jPanel.add(getExitButton(), BorderLayout.SOUTH);

        return jPanel;
    }

    private JPanel getExitButton() {
        JPanel panel = new JPanel();
        JButton button = new JButton("Exit");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                dispose();
            }
        });
        panel.add(button);

        return panel;
    }

    private void initJMenuBar() {

        // Create menubar
        jMenuBar = new JMenuBar();

        // File menu
        jMenuBar.add(createFileMenu());

        // Run edit menu
        jMenuBar.add(createRunEditMenu());
        
        // Activity type edit menu
        jMenuBar.add(createTypeEditMenu());

        // Delete menu
        jMenuBar.add(createDeleteMenu());

        // Create style switcher
        jMenu = (JMenu) jMenuBar.add(new JMenu(("Change style")));

        jMenuItem = createStylesMenuItem(jMenu, "Nimbus style", nimbus);
        jMenuItem.setSelected(true); // this is the default style

        jMenuItem = createStylesMenuItem(jMenu, "Metal style", metal);

        UIManager.LookAndFeelInfo[] lafInfo = UIManager.
                getInstalledLookAndFeels();

        for (int counter = 0; counter < lafInfo.length; counter++) {
            String className = lafInfo[counter].getClassName();
            switch (className) {
                case motif:
                    createStylesMenuItem(jMenu, "Motif style", motif);
                    break;
                case windows:
                    createStylesMenuItem(jMenu, "Windows style", windows);
                    break;
                case gtk:
                    createStylesMenuItem(jMenu, "Gtk style", gtk);
                    break;
                case mac:
                    createStylesMenuItem(jMenu, "Mac style", mac);
                    break;
            }
        }

        String styleName = getLookAndFeelLabel(currentLookAndFeel);

        for (int i = 0; i < jMenu.getItemCount(); i++) {
            JMenuItem item = jMenu.getItem(i);
            item.setSelected(item.getText().equals(styleName));
        }

        this.setJMenuBar(jMenuBar);
    }

    private JMenu createDeleteMenu() {

        jMenu = new JMenu("Delete activies");

        jMenuItem = new JMenuItem("Delete one activity");
        jMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
            }
        });
        jMenu.add(jMenuItem);


        jMenuItem = new JMenuItem("Delete all activities");
        jMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
            }
        });
        jMenu.add(jMenuItem);

        return jMenu;
    }

    private JMenu createFileMenu() {

        jMenu = new JMenu("File");

        jMenuItem = new JMenuItem("Load");
        jMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    final JFileChooser fc = new JFileChooser();

                    int response = fc.showOpenDialog(Mainframe.this);

                    if (response == JFileChooser.APPROVE_OPTION) {
                        File file = fc.getSelectedFile();

                        FileUtils.getInstance().importDatabase(file.getPath());
                        
                        JOptionPane.showMessageDialog(Mainframe.this,
                                "Activities loaded.",
                                "Information Message",
                                JOptionPane.INFORMATION_MESSAGE);

                    } else {
                    }

                } catch (HeadlessException | IOException | ClassNotFoundException ex) {
                    JOptionPane.showMessageDialog(Mainframe.this,
                            "Cannot load file.",
                            "Error Message",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        jMenu.add(jMenuItem);

        jMenuItem = new JMenuItem("Save");
        jMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                try {

                    FileUtils.getInstance().saveDatabase("Activities.dat");
                    
                    JOptionPane.showMessageDialog(Mainframe.this,
                                "Activities saved.",
                                "Information Message",
                                JOptionPane.INFORMATION_MESSAGE);
                    
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(Mainframe.this,
                            "Cannot save file.",
                            "Error Message",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        jMenu.add(jMenuItem);

        jMenuItem = new JMenuItem("Save as...");
        jMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    final JFileChooser fc = new JFileChooser();

                    int response = fc.showSaveDialog(Mainframe.this);

                    if (response == JFileChooser.APPROVE_OPTION) {

                        File file = fc.getSelectedFile();

                        FileUtils.getInstance().saveDatabase(file.getPath());

                        JOptionPane.showMessageDialog(Mainframe.this,
                                "Activities saved.",
                                "Information Message",
                                JOptionPane.INFORMATION_MESSAGE);

                    }

                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(Mainframe.this,
                            "Cannot save file.",
                            "Error Message",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        jMenu.add(jMenuItem);

        jMenu.addSeparator();

        submenu = new JMenu("Print");
        jMenuItem = new JMenuItem("Export to text file");
        jMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                try {

                    final JFileChooser fc = new JFileChooser();

                    int response = fc.showSaveDialog(Mainframe.this);

                    if (response == JFileChooser.APPROVE_OPTION) {
                        File file = fc.getSelectedFile();

                        FileUtils.getInstance().saveTablesToTxt();

                        JOptionPane.showMessageDialog(Mainframe.this,
                                "Activities exported to text files.",
                                "Information Message",
                                JOptionPane.INFORMATION_MESSAGE);

                    } else {
                    }
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(Mainframe.this,
                            "Cannot export file.",
                            "Error Message",
                            JOptionPane.ERROR_MESSAGE);

                    Logger.getLogger(Mainframe.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        submenu.add(jMenuItem);

        jMenu.add(submenu);

        return jMenu;
    }

    private JMenu createRunEditMenu() {

        jMenu = new JMenu("Edit activities");

        jMenuItem = new JMenuItem("Add new activity");
        jMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {

                RunDialog activityDialog = new NewRunDialog(Mainframe.this);

            }
        });
        jMenu.add(jMenuItem);

        jMenuItem = new JMenuItem("Edit activity");
        jMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
            }
        });
        jMenu.add(jMenuItem);

        return jMenu;
    }
    
    private JMenu createTypeEditMenu() {

        jMenu = new JMenu("Edit activity types");

        jMenuItem = new JMenuItem("Add new activity type");
        jMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {

                TypeDialog typeDialog = new NewTypeDialog(Mainframe.this);

            }
        });
        jMenu.add(jMenuItem);

        jMenuItem = new JMenuItem("Delete activity type");
        jMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                
                DeleteTypeDialog typeDialog = new DeleteTypeDialog(Mainframe.this);
            }
        });
        jMenu.add(jMenuItem);

        return jMenu;
    }

    /**
     * Creates a JRadioButtonMenuItem for the Look and Feel menu
     */
    public JMenuItem createStylesMenuItem(JMenu jMenu, String label, String style) {
        JMenuItem menuItem = (JRadioButtonMenuItem) jMenu.add(new JRadioButtonMenuItem(label));
        styleMenu.add(menuItem);
        menuItem.addActionListener(new ChangeLookAndFeelAction(style));

        menuItem.setEnabled(isAvailableLookAndFeel(style));

        return menuItem;
    }

    private JComboBox getSearchBox() {
        searchBox = new JComboBox(searchBy);
        searchBox.addItemListener(this);

        return searchBox;
    }

    private JPopupMenu getPopupMenu() {

        // constructs the popup menu
        jPopupMenu = new JPopupMenu();
        jMenuItemAdd = new JMenuItem("New activity...");
        jMenuItemEdit = new JMenuItem("Edit activity...");
        jMenuItemRemove = new JMenuItem("Delete activity...");
        jMenuItemRemoveAll = new JMenuItem("Delete all activities");

        jMenuItemAdd.addActionListener(this);
        jMenuItemEdit.addActionListener(this);
        jMenuItemRemove.addActionListener(this);
        jMenuItemRemoveAll.addActionListener(this);

        jPopupMenu.add(jMenuItemAdd);
        jPopupMenu.add(jMenuItemEdit);
        jPopupMenu.add(jMenuItemRemove);
        jPopupMenu.add(jMenuItemRemoveAll);

        return jPopupMenu;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        JMenuItem menu = (JMenuItem) event.getSource();
        if (menu == jMenuItemAdd) {
            addNewRow();
        } else if (menu == jMenuItemEdit) {
            editCurrentRow();
        } else if (menu == jMenuItemRemove) {
            removeCurrentRow();
        } else if (menu == jMenuItemRemoveAll) {
            removeAllRows();
        }
    }

    private void addNewRow() {
        RunDialog activityDialog = new NewRunDialog(Mainframe.this);
    }

    protected void createNewRow(Calendar dateTime, String desc,
            int sessionTime, float distInKm,
            int avrgHeartRate, TypeEntity type) {

        // Forward data to table model class
        resultModel.addRow(dateTime, desc, sessionTime,
                distInKm, avrgHeartRate, type);
    }

    private void editCurrentRow() {
        RunDialog activityDialog = new EditRunDialog(this, getRow());
    }

    private RunEntity getRow() {

        return resultModel.getEntity(jTable.getSelectedRow());
    }

    protected void editRow(Calendar dateTime, String desc,
            int sessionTime, float distInKm,
            int avrgHeartRate, TypeEntity type) {

        // Forward data to table model class
        resultModel.updateRow(dateTime, desc, sessionTime,
                distInKm, avrgHeartRate, type);
    }

    private void removeCurrentRow() {

        int option = JOptionPane.showConfirmDialog(
                this, "Are you sure you want delete the activity?",
                "Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (option == JOptionPane.YES_OPTION) {

            // Delete row
            int id = resultModel.getId(jTable.getSelectedRow());
            resultModel.removeRow(id);
        } else if (option == JOptionPane.NO_OPTION) {
        } else {
        }
    }

    private void removeAllRows() {
        int option = JOptionPane.showConfirmDialog(
                this, "Are you sure you want to delete all activities?",
                "Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (option == JOptionPane.YES_OPTION) {

            // Delete table
            resultModel.removeAllRows();
        } else if (option == JOptionPane.NO_OPTION) {
        } else {
        }
    }
    
    protected void addNewType() {
        
    }
    
    protected void editType() {
        
    }
    
    protected void deleteType() {
        
    }

    private void filter() {
        RowFilter<ResultSetTableModel, Object> rf = null;
        //If current expression doesn't parse, don't update.
        try {
            rf = RowFilter.regexFilter(filterText.getText(), selection);
        } catch (java.util.regex.PatternSyntaxException e) {
            return;
        }
        sorter.setRowFilter(rf);
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        switch (searchBox.getSelectedItem().toString()) {
            case "Search by description":
                selection = 1;
                break;
            case "Search by session time":
                selection = 2;
                break;
            case "Search by distance":
                selection = 3;
                break;
            case "Search by heart rate":
                selection = 4;
                break;
            case "Search by activity type":
                selection = 5;
                break;
        }
    }

    private void initLookAndFeel() {
        try {
            for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException |
                IllegalAccessException | UnsupportedLookAndFeelException e) {
            
            // If Nimbus is not available, you can set the GUI to another look and feel.
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (ClassNotFoundException | InstantiationException |
                    IllegalAccessException | UnsupportedLookAndFeelException err) {
                System.err.println("Unknown exception: " + e + "and " + err);
            }
        }
    }

    /**
     * A utility function that layers on top of the LookAndFeel's
     * isSupportedLookAndFeel() method. Returns true if the LookAndFeel is
     * supported. Returns false if the LookAndFeel is not supported and/or if
     * there is any kind of error checking if the LookAndFeel is supported.
     *
     * The L&F menu will use this method to detemine whether the various L&F
     * options should be active or inactive.
     *
     */
    protected boolean isAvailableLookAndFeel(String style) {
        try {
            Class lnfClass = Class.forName(style);
            LookAndFeel newLAF = (LookAndFeel) (lnfClass.newInstance());
            return newLAF.isSupportedLookAndFeel();
        } catch (Exception e) { // If ANYTHING weird happens, return false
            return false;
        }
    }

    /**
     * Stores the current L&F, and calls updateLookAndFeel, below
     */
    public void setLookAndFeel(String style) {
        if (!currentLookAndFeel.equals(style)) {
            currentLookAndFeel = style;

            String styleName = getLookAndFeelLabel(style);
            //themesMenu.setEnabled(style.equals(metal));
            updateLookAndFeel();
            for (int i = 0; i < jMenu.getItemCount(); i++) {
                JMenuItem item = jMenu.getItem(i);
                item.setSelected(item.getText().equals(styleName));
            }
        }
    }

    private String getLookAndFeelLabel(String style) {

        switch (style) {
            case mac:
                return "Mac";
            case metal:
                return "Java";
            case nimbus:
                return "Nimbus";
            case motif:
                return "Motif";
            case windows:
                return "Windows";
            case gtk:
                return "Gtk";
            default:
                throw new RuntimeException("Unsupported Look and Feel: " + style);
        }
    }

    /**
     * Sets the current L&F on each demo module
     */
    public void updateLookAndFeel() {
        try {
            UIManager.setLookAndFeel(currentLookAndFeel);

            SwingUtilities.updateComponentTreeUI(this);

        } catch (ClassNotFoundException | InstantiationException 
                | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            System.out.println("Failed loading L&F: " + currentLookAndFeel);
            System.out.println(ex);
        }
    }

    class ChangeLookAndFeelAction extends AbstractAction {

        String style;

        protected ChangeLookAndFeelAction(String style) {
            super("ChangeTheme");
            this.style = style;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            setLookAndFeel(style);
        }
    }

    private void setCenterLocation() {
        dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int centerX = (int) ((dimension.getWidth() - getWidth()) / 2);
        int centerY = (int) ((dimension.getHeight() - getHeight()) / 2);
        setLocation(centerX, centerY);
    }

    public class TableScroller extends ComponentAdapter {

        @Override
        public void componentResized(ComponentEvent event) {
            int lastRow = resultModel.getRowCount() - 1;
            int cellTop = jTable.getCellRect(lastRow, 0, true).y;
            JScrollPane jsp = (JScrollPane) 
                    SwingUtilities.getAncestorOfClass(JScrollPane.class, jTable);
            JViewport jvp = jsp.getViewport();
            int portHeight = jvp.getSize().height;
            int position = cellTop - 
                    (portHeight - jTable.getRowHeight() - jTable.getRowMargin());
            if (position >= 0) {
                jvp.setViewPosition(new Point(0, position));
            }
        }
    }

    public class TableMouseListener extends MouseAdapter {

        private JTable jTable;

        public TableMouseListener(JTable jTable) {
            this.jTable = jTable;
        }

        @Override
        public void mousePressed(MouseEvent event) {
           
            // selects the row at which point the mouse is clicked
            Point point = event.getPoint();
            int currentRow = jTable.rowAtPoint(point);
            jTable.setRowSelectionInterval(currentRow, currentRow);
        }
    }
}