package cz.cvut.fel.skoumond.gui;

import cz.cvut.fel.skoumond.entities.TypeEntity;
import cz.cvut.fel.skoumond.exceptions.NoConnectionException;
import cz.cvut.fel.skoumond.sql.TypeRepository;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;

/**
 *
 * @author Ondra
 */
public class DeleteTypeDialog extends JDialog {

    private JTextField typeField;
    
    private JPanel jPanel;
    private JLabel jLabel;
    private JComboBox<TypeEntity> jComboBox;
    private List<TypeEntity> arrayList = TypeRepository.getInstance().all();
    private TypeEntity activityType;
    
    private Mainframe mainframe;

    public DeleteTypeDialog(Mainframe mainframe) {
        super(mainframe, "Edit activity types", true);

        Container pane = getContentPane();
        pane.setLayout(new BorderLayout());

        pane.add(getJPanel(), BorderLayout.NORTH);
        pane.add(new JSeparator(), BorderLayout.CENTER);
        pane.add(getButtonPanel(), BorderLayout.SOUTH);

        this.mainframe = mainframe;

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private JPanel getJPanel() {

        GridLayout gridLayout = new GridLayout(1, 2);
        setResizable(false);

        jPanel = new JPanel();
        jPanel.setLayout(gridLayout);

        jLabel = new JLabel(" Choose the activity type: ", JLabel.LEFT);
        jPanel.add(jLabel);

        jPanel.add(getComboBox());

        return jPanel;
    }
    
    private JComboBox getComboBox() {

        jComboBox = new JComboBox<>();

        for (TypeEntity typeEntity : arrayList) {
            jComboBox.addItem(typeEntity);
        }

        jComboBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent ie) {
                activityType = jComboBox.getItemAt(jComboBox.getSelectedIndex());
            }
        });
        return jComboBox;
    }

    private JPanel getButtonPanel() {
        JPanel panel = new JPanel();
        JButton jButton = new JButton("Ok");

        jButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                onDialogOk();
            }
        });
        panel.add(jButton);
        jButton = new JButton("Back");
        jButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                onDialogCancel();
            }
        });
        panel.add(jButton);
        return panel;
    }

    private void onDialogOk() {
        try {
            
            TypeRepository.getInstance().delete((TypeEntity) jComboBox.getSelectedItem());

            JOptionPane.showMessageDialog(this,
                    "Activity type was deleted.",
                    "Information Message",
                    JOptionPane.INFORMATION_MESSAGE);

            setVisible(false);
            dispose();

        } catch (NumberFormatException | NoConnectionException ex) {
            JOptionPane.showMessageDialog(this,
                    "Bad entered information. Try it again.",
                    "Error Message",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
    
    private void onDialogCancel() {
        setVisible(false);
        dispose();
    }

    public JTextField getTypeField() {
        return typeField;
    }
    
}
