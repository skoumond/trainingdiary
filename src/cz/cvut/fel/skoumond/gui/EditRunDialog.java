
package cz.cvut.fel.skoumond.gui;

import cz.cvut.fel.skoumond.entities.RunEntity;
import cz.cvut.fel.skoumond.exceptions.NoConnectionException;
import cz.cvut.fel.skoumond.utilities.DateReader;
import javax.swing.JOptionPane;

/**
 *
 * @author Ondra
 */
public class EditRunDialog extends RunDialog {
     
    private RunEntity entity;
    
    public EditRunDialog(Mainframe mainframe, RunEntity entity) {
        super(mainframe); 
        
        this.entity = entity;
        
        presetFields();
        
        this.setVisible(true);
    }

    private void presetFields() {
        
        super.getDateField().setText(DateReader.formatData.format(entity.getDateTime().getTime()));
        super.getDescField().setText(entity.getDesc());
        super.getSessionTimeField().setText(String.valueOf(entity.getSessionTime()));
        super.getDistInKmField().setText(String.valueOf(entity.getDistInKm()));
        super.getAvrgHeartRateField().setText(String.valueOf(entity.getAvrgHeartRate()));
    }

    @Override
    protected void onDialogOk() {
        try {
            super.getMainframe().editRow(
                    super.getCalendar(),
                    super.getDescField().getText(),
                    Integer.parseInt(super.getSessionTimeField().getText()),
                    Float.parseFloat(super.getDistInKmField().getText()),
                    Integer.parseInt(super.getAvrgHeartRateField().getText()),
                    super.getActivityType());

            JOptionPane.showMessageDialog(this,
                    "Activity edited.",
                    "Information Message",
                    JOptionPane.INFORMATION_MESSAGE);

            setVisible(false);
            dispose();

        } catch (NumberFormatException | NoConnectionException ex) {
            JOptionPane.showMessageDialog(this,
                    "Bad entered information. Try it again.",
                    "Error Message",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
}
