/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.skoumond.gui;

import cz.cvut.fel.skoumond.entities.TypeEntity;
import cz.cvut.fel.skoumond.exceptions.NoConnectionException;
import cz.cvut.fel.skoumond.sql.TypeRepository;
import javax.swing.JOptionPane;

/**
 *
 * @author Ondra
 */
public class NewTypeDialog extends TypeDialog {
    
    public NewTypeDialog(Mainframe mainframe) {
        super(mainframe);
        
        setVisible(true);
    }
    
    @Override
    protected void onDialogOk() {
        try {
            
            TypeRepository.getInstance().save(new TypeEntity(
                    TypeRepository.getInstance().getFreeId(), 
                    super.getTypeField().getText()));

            JOptionPane.showMessageDialog(this,
                    "New activity type added.",
                    "Information Message",
                    JOptionPane.INFORMATION_MESSAGE);

            setVisible(false);
            dispose();

        } catch (NumberFormatException | NoConnectionException ex) {
            JOptionPane.showMessageDialog(this,
                    "Bad entered information. Try it again.",
                    "Error Message",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
}
