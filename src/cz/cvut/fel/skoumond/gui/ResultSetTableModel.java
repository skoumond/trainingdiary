
package cz.cvut.fel.skoumond.gui;

import cz.cvut.fel.skoumond.utilities.DateReader;
import cz.cvut.fel.skoumond.entities.RunEntity;
import cz.cvut.fel.skoumond.entities.TypeEntity;
import cz.cvut.fel.skoumond.sql.RunRepository;
import java.util.Calendar;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Ondra
 */
class ResultSetTableModel extends AbstractTableModel {

    private RunRepository repository;
    
    private final String[] columnNames = {
        "Date", 
        "Description",
        "Session time", 
        "Distance", 
        "HeartRate", 
        "Activity type"
    };
    
    private List<RunEntity> arrayList;
    
    private int row;

    public ResultSetTableModel() {
        repository = RunRepository.getInstance();
        arrayList = repository.all();
    }
    
    @Override
    public int getRowCount() {
        return arrayList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int row, int column) {
        Object out = "";
        switch (column) {
            case (0):
                out = DateReader.formatData.format(
                        arrayList.get(row).getDateTime().getTime());
                break;
            case (1):
                out = arrayList.get(row).getDesc();
                break;
            case (2):
                out = arrayList.get(row).getSessionTime();
                break;
            case (3):
                out = arrayList.get(row).getDistInKm();
                break;
            case (4):
                out = arrayList.get(row).getAvrgHeartRate();
                break;
            case (5):
                out = arrayList.get(row).getType().getType();
                break;
        }
        return out;
    }
    
    public void addRow(Calendar dateTime, String desc, 
            int sessionTime, float distInKm, 
            int avrgHeartRate, TypeEntity type) {
        
        // Create new run object
        RunEntity entity = new RunEntity(
                    RunRepository.getInstance().getFreeId(),
                    dateTime, desc, sessionTime, distInKm,
                    avrgHeartRate, type
                );    
        // Update actual tablemodel
        arrayList.add(entity);
        
        // Update database
        repository.save(entity);
        
        // Get database know something happened
        fireTableDataChanged();
    }
    
    public int getId(int row) {
        return arrayList.get(row).getId();
    }
    
    public RunEntity getEntity(int row) {
        
        this.row = row;
        
        return arrayList.get(row);
    }
    
    public void updateRow(Calendar dateTime, String desc, 
            int sessionTime, float distInKm, 
            int avrgHeartRate, TypeEntity type) {
        
        // Get run object
        RunEntity entity = arrayList.get(row);
        
        // Update actual tablemodel
        entity.setDateTime(dateTime);
        entity.setDesc(desc);
        entity.setSessionTime(sessionTime);
        entity.setDistInKm(distInKm);
        entity.setAvrgHeartRate(avrgHeartRate);
        entity.setType(type);
        
        // Update database
        repository.update(entity);
        
        // Get database know something happened
        fireTableDataChanged();
    }
    
    public void removeRow(int id) {
        RunEntity tmp = null;
        
        for (RunEntity entity : arrayList) {
            if (entity.getId() == id) {
                tmp = entity;
                break;
            }
        }
        // Update actual tablemodel
        arrayList.remove(tmp);
        
        // Update database
        repository.delete(tmp);
        
        // Get database know something happened
        fireTableDataChanged();
    }
    
    public void removeAllRows() {
        
        // Update actual tablemodel
        arrayList.clear();
        
        // Update database
        repository.deleteAll();
        
        // Get database know something happened
        fireTableDataChanged();
    }
    
    public void newType() {
        
    }
    
    public void editType() {
        
    }
    
    

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }
}
