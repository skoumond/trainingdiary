package cz.cvut.fel.skoumond.gui;

import cz.cvut.fel.skoumond.utilities.DateReader;
import cz.cvut.fel.skoumond.entities.RunEntity;
import cz.cvut.fel.skoumond.entities.TypeEntity;
import cz.cvut.fel.skoumond.sql.TypeRepository;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Calendar;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;

/**
 *
 * @author Ondra
 */
public abstract class RunDialog extends JDialog {

    private JLabel dateTime;
    private JLabel desc;
    private JLabel sessionTime;
    private JLabel distInKm;
    private JLabel avrgHeartRate;
    private JTextField dateField;
    private JTextField descField;
    private JTextField sessionTimeField;
    private JTextField distInKmField;
    private JTextField avrgHeartRateField;
    //private JButton jButton;
    private JComboBox<TypeEntity> jComboBox;
    private List<TypeEntity> arrayList = TypeRepository.getInstance().all();
    private TypeEntity activityType;
    private RunEntity entity;
    private JPanel jPanel;
    private JLabel jLabel;
    final static int maxGap = 50;
    private Mainframe mainframe;

    public RunDialog(Mainframe mainframe) {
        super(mainframe, "Edit activities", true);


        Container pane = getContentPane();
        pane.setLayout(new BorderLayout());

        pane.add(getJPanel(), BorderLayout.NORTH);
        pane.add(new JSeparator(), BorderLayout.CENTER);
        pane.add(getButtonPanel(), BorderLayout.SOUTH);

        this.mainframe = mainframe;

        //setSize(100, 200);
        pack();
        setLocationRelativeTo(null);
    }

    private JPanel getJPanel() {

        GridLayout gridLayout = new GridLayout(6, 2);
        setResizable(false);

        jPanel = new JPanel();
        jPanel.setLayout(gridLayout);

        JLabel cellWidth = new JLabel("********************");
        Dimension labelsize = cellWidth.getPreferredSize();
        jPanel.setPreferredSize(new Dimension((int) (labelsize.getWidth() * 2.5) + maxGap,
                (int) (labelsize.getHeight() * 3.5) + maxGap * 2));

        jLabel = new JLabel(" Enter date and time: ", JLabel.LEFT);
        jPanel.add(jLabel);
        dateField = new JTextField(10);
        jLabel.setLabelFor(dateField);
        jPanel.add(dateField);

        jLabel = new JLabel(" Enter description: ", JLabel.LEFT);
        jPanel.add(jLabel);
        descField = new JTextField(10);
        jLabel.setLabelFor(descField);
        jPanel.add(descField);

        jLabel = new JLabel(" Enter activity lenght: ", JLabel.LEFT);
        jPanel.add(jLabel);
        sessionTimeField = new JTextField(10);
        jLabel.setLabelFor(sessionTimeField);
        jPanel.add(sessionTimeField);

        jLabel = new JLabel(" Enter running distance (Km) :", JLabel.LEFT);
        jPanel.add(jLabel);
        distInKmField = new JTextField(10);
        jLabel.setLabelFor(distInKmField);
        jPanel.add(distInKmField);

        jLabel = new JLabel(" Enter average heart rate: ", JLabel.LEFT);
        jPanel.add(jLabel);
        avrgHeartRateField = new JTextField(10);
        jLabel.setLabelFor(avrgHeartRateField);
        jPanel.add(avrgHeartRateField);


        jLabel = new JLabel(" Choose the activity type: ", JLabel.LEFT);
        jPanel.add(jLabel);

        jPanel.add(getComboBox());

        return jPanel;
    }

    private JComboBox getComboBox() {

        jComboBox = new JComboBox<>();

        for (TypeEntity typeEntity : arrayList) {
            jComboBox.addItem(typeEntity);
        }

        jComboBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent ie) {
                activityType = jComboBox.getItemAt(jComboBox.getSelectedIndex());
            }
        });
        return jComboBox;
    }

    private JPanel getButtonPanel() {
        JPanel panel = new JPanel();
        JButton jButton = new JButton("Ok");

        jButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                onDialogOk();
            }
        });
        panel.add(jButton);
        jButton = new JButton("Back");
        jButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                onDialogCancel();
            }
        });
        panel.add(jButton);
        return panel;
    }

    protected abstract void onDialogOk();

    private void onDialogCancel() {
        setVisible(false);
        dispose();
    }

    public RunEntity getEntity() {
        return entity;
    }

    protected Calendar getCalendar() {
        return new DateReader().readDateTime(dateField.getText());
    }

    // *** GETTERS AND SETTERS ***
    public JLabel getDateTime() {
        return dateTime;
    }

    public void setDateTime(JLabel dateTime) {
        this.dateTime = dateTime;
    }

    public JLabel getDesc() {
        return desc;
    }

    public void setDesc(JLabel desc) {
        this.desc = desc;
    }

    public JLabel getSessionTime() {
        return sessionTime;
    }

    public void setSessionTime(JLabel sessionTime) {
        this.sessionTime = sessionTime;
    }

    public JLabel getDistInKm() {
        return distInKm;
    }

    public void setDistInKm(JLabel distInKm) {
        this.distInKm = distInKm;
    }

    public JLabel getAvrgHeartRate() {
        return avrgHeartRate;
    }

    public void setAvrgHeartRate(JLabel avrgHeartRate) {
        this.avrgHeartRate = avrgHeartRate;
    }

    public JTextField getDateField() {
        return dateField;
    }

    public void setDateField(JTextField dateField) {
        this.dateField = dateField;
    }

    public JTextField getDescField() {
        return descField;
    }

    public void setDescField(JTextField descField) {
        this.descField = descField;
    }

    public JTextField getSessionTimeField() {
        return sessionTimeField;
    }

    public void setSessionTimeField(JTextField sessionTimeField) {
        this.sessionTimeField = sessionTimeField;
    }

    public JTextField getDistInKmField() {
        return distInKmField;
    }

    public void setDistInKmField(JTextField distInKmField) {
        this.distInKmField = distInKmField;
    }

    public JTextField getAvrgHeartRateField() {
        return avrgHeartRateField;
    }

    public void setAvrgHeartRateField(JTextField avrgHeartRateField) {
        this.avrgHeartRateField = avrgHeartRateField;
    }

    public JComboBox<TypeEntity> getjComboBox() {
        return jComboBox;
    }

    public void setjComboBox(JComboBox<TypeEntity> jComboBox) {
        this.jComboBox = jComboBox;
    }

    public List<TypeEntity> getArrayList() {
        return arrayList;
    }

    public void setArrayList(List<TypeEntity> arrayList) {
        this.arrayList = arrayList;
    }

    public TypeEntity getActivityType() {
        return activityType;
    }

    public void setActivityType(TypeEntity activityType) {
        this.activityType = activityType;
    }

    public JPanel getjPanel() {
        return jPanel;
    }

    public void setjPanel(JPanel jPanel) {
        this.jPanel = jPanel;
    }

    public JLabel getjLabel() {
        return jLabel;
    }

    public void setjLabel(JLabel jLabel) {
        this.jLabel = jLabel;
    }

    public Mainframe getMainframe() {
        return mainframe;
    }

    public void setMainframe(Mainframe mainframe) {
        this.mainframe = mainframe;
    }
}