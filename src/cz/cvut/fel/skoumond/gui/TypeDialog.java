package cz.cvut.fel.skoumond.gui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;

/**
 *
 * @author Ondra
 */
public abstract class TypeDialog extends JDialog {

    private JTextField typeField;
    
    private JPanel jPanel;
    private JLabel jLabel;
    final static int maxGap = 3;
    private Mainframe mainframe;

    public TypeDialog(Mainframe mainframe) {
        super(mainframe, "Edit activity types", true);

        Container pane = getContentPane();
        pane.setLayout(new BorderLayout());

        pane.add(getJPanel(), BorderLayout.NORTH);
        pane.add(new JSeparator(), BorderLayout.CENTER);
        pane.add(getButtonPanel(), BorderLayout.SOUTH);

        this.mainframe = mainframe;

        pack();
        setLocationRelativeTo(null);
    }

    private JPanel getJPanel() {

        GridLayout gridLayout = new GridLayout(1, 2);
        setResizable(false);

        jPanel = new JPanel();
        jPanel.setLayout(gridLayout);

        jLabel = new JLabel(" Enter activity type desription: ", JLabel.LEFT);
        jPanel.add(jLabel);
        typeField = new JTextField(10);
        jLabel.setLabelFor(typeField);
        jPanel.add(typeField);

        return jPanel;
    }

    private JPanel getButtonPanel() {
        JPanel panel = new JPanel();
        JButton jButton = new JButton("Ok");

        jButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                onDialogOk();
            }
        });
        panel.add(jButton);
        jButton = new JButton("Back");
        jButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                onDialogCancel();
            }
        });
        panel.add(jButton);
        return panel;
    }

    protected abstract void onDialogOk();
    
    private void onDialogCancel() {
        setVisible(false);
        dispose();
    }

    public JTextField getTypeField() {
        return typeField;
    }
    
}
