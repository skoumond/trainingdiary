
package cz.cvut.fel.skoumond.gui;

import cz.cvut.fel.skoumond.exceptions.NoConnectionException;
import javax.swing.JOptionPane;

/**
 *
 * @author Ondra
 */
public class NewRunDialog extends RunDialog {

    public NewRunDialog(Mainframe mainframe) {
        super(mainframe);
        
        setVisible(true);
    }
    
    @Override
    protected void onDialogOk() {
        try {
            super.getMainframe().createNewRow(
                    super.getCalendar(),
                    super.getDescField().getText(),
                    Integer.parseInt(super.getSessionTimeField().getText()),
                    Float.parseFloat(super.getDistInKmField().getText()),
                    Integer.parseInt(super.getAvrgHeartRateField().getText()),
                    super.getActivityType());

            JOptionPane.showMessageDialog(this,
                    "New activity added.",
                    "Information Message",
                    JOptionPane.INFORMATION_MESSAGE);

            setVisible(false);
            dispose();

        } catch (NumberFormatException | NoConnectionException ex) {
            JOptionPane.showMessageDialog(this,
                    "Bad entered information. Try it again.",
                    "Error Message",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
}
