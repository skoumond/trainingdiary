
package cz.cvut.fel.skoumond.sql;

import java.util.List;

/**
 * Rozhraní definující práci repozitářů s databází.
 * 
 * @author Ondra
 */
public interface IRepository<T> {

    /**
     * Vstupem typ objektu který hledáme.
     * Vrací nalezený objekt.
     * 
     * @param index
     * @return generické T
     */
    public T findById(int index);

    /**
     * Vrací všechny prvky databáze.
     * 
     * @return generický List<T>
     */
    public List<T> all();

    /**
     * Ukládá danou entitu.
     * 
     * @param entity 
     */
    public void save(T entity);

    /**
     * Upravuje daný záznam.
     * 
     * @param entity 
     */
    public void update(T entity);

    /**
     * Maže daný záznam.
     * 
     * @param entity 
     */
    public void delete(T entity);
    
    public void deleteAll();
    
    public int getFreeId();
}
