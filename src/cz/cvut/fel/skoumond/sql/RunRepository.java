package cz.cvut.fel.skoumond.sql;

import cz.cvut.fel.skoumond.entities.RunEntity;
import cz.cvut.fel.skoumond.entities.TypeEntity;
import cz.cvut.fel.skoumond.exceptions.DataEntryException;
import cz.cvut.fel.skoumond.exceptions.NoConnectionException;
import java.sql.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 *
 * Třída tvořící repozitář záznámů.
 * 
 * @author Ondra
 */
public class RunRepository extends Repository<RunEntity> {

    private static RunRepository instance = null;
    /**
     *  Příkaz SQL pro hledání podle identifikačního čísla .
     */
    private final String FINDBYID = "SELECT * FROM APP.RUN WHERE R_ID = ?";
    /**
     *  Příkaz SQL pro vyhledání všech záznamů.
     */
    private final String ALL = "SELECT * FROM APP.RUN";
    
    private final String JOIN = "SELECT * FROM APP.RUN JOIN APP.TYPE ON APP.RUN.T_ID = APP.TYPE.T_ID";
    
    /**
     *  Příkaz SQL pro vložení nových dat do databáze.
     */
    private final String INSERT = "INSERT INTO APP.RUN VALUES (?, ?, ?, ?, ?, ?, ?)";
    /**
     *  Příkaz SQL pro aktualizaci již uložených dat do databáze.
     */
    private final String UPDATE = "UPDATE APP.RUN SET R_CALENDAR = ?, R_DESCRIPTION = ?, R_SESSIONTIME = ?, R_DISTANCE = ?, R_AVRGHEARTRATE = ?, T_ID = ? WHERE R_ID = ?";
    /**
     *  Příkaz SQL pro smazání daných dat z databáze.
     */
    private final String DELETE = "DELETE FROM APP.RUN WHERE R_ID = ?";
    
    private final String DELETEALL = "DELETE FROM APP.RUN";
    
    private final String GET_ID = "SELECT R_ID FROM APP.RUN ORDER BY R_ID";

    private RunRepository() {
        entities = new HashMap();
    }
    
    public static RunRepository getInstance() {
        if (instance == null) {
            instance = new RunRepository();
        }
        return instance;
    }
    
    /**
     * Najde záznam pomocí identifikačního čísla.
     * 
     * @param index
     * @return RunEntity
     */
    @Override
    public RunEntity findById(int index) {

        if (this.entities.containsKey(index)) {
            return this.entities.get(index);
        }

        try (Connection con = getConnection(); 
                PreparedStatement ps = con.prepareStatement(FINDBYID)) {
            //DriverManager.setLogWriter(new PrintWriter(new BufferedOutputStream(System.out)));
            ps.setInt(1, index);
            ResultSet rs = ps.executeQuery();
            rs.next();
            long time = rs.getTimestamp(2).getTime();
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(time);
            
            return new RunEntity(rs.getInt(1), calendar, rs.getString(3), 
                    rs.getInt(4), rs.getFloat(5), rs.getInt(6), 
                    TypeRepository.getInstance().findById(rs.getInt(7)));

        } catch (SQLException e) {
            System.err.println("Chyba při komunikaci s databází: " + e.getMessage());
            throw new NoConnectionException(e.getMessage());
        }
    }

    /**
     * Vrací kolekci všech záznamů v databázi.
     * 
     * @return List<RunEntity> 
     * @throws NoConnectionException 
     */
    @Override
    public List<RunEntity> all() throws NoConnectionException {

        List<RunEntity> arrayList = new ArrayList();

        try (Connection con = getConnection(); 
                PreparedStatement ps = con.prepareStatement(ALL)) {
            //DriverManager.setLogWriter(new PrintWriter(new BufferedOutputStream(System.out)));
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                long time = rs.getTimestamp(2).getTime();
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(time);
                RunEntity entity = new RunEntity(rs.getInt(1), 
                        calendar, rs.getString(3), rs.getInt(4), 
                        rs.getFloat(5), rs.getInt(6), 
                        TypeRepository.getInstance().findById(rs.getInt(7)));
                arrayList.add(entity);
                entities.put(entity.getId(), entity);
            }
            return arrayList;
        } catch (SQLException e) {
            System.err.println("Chyba při komunikaci s databází: " + e.getMessage());
            throw new NoConnectionException(e.getMessage());
        }
    }

    /**
     * Uloží daný záznam do databáze.
     * 
     * @param RunEntity
     * @throws NoConnectionException 
     */
    @Override
    public void save(RunEntity entity) throws NoConnectionException {
        
        try (Connection con = getConnection(); PreparedStatement ps = con.prepareStatement(INSERT)) {
            //DriverManager.setLogWriter(new PrintWriter(new BufferedOutputStream(System.out)));
            ps.setInt(1, entity.getId());
            ps.setTimestamp(2, new java.sql.Timestamp(entity.getDateTime().getTimeInMillis()));   
            ps.setString(3, entity.getDesc());
            ps.setInt(4, entity.getSessionTime());
            ps.setFloat(5, entity.getDistInKm());
            ps.setInt(6, entity.getAvrgHeartRate());
            ps.setInt(7, entity.getType().getId());
            ps.executeUpdate();
            this.entities.put(entity.getId(), entity);

        } catch (SQLException e) {
            System.err.println("Chyba při komunikaci s databází: " + e.getMessage());
            throw new NoConnectionException(e.getMessage());
        }
    }

    /**
     * Aktualizace již uložených dat v databázi.
     * 
     * @param RunEntity
     * @throws NoConnectionException 
     */
    @Override
    public void update(RunEntity entity) throws NoConnectionException {

        try (Connection con = getConnection();) {

            con.setAutoCommit(false);
            try (PreparedStatement ps = con.prepareStatement(UPDATE)) {
                //DriverManager.setLogWriter(new PrintWriter(new BufferedOutputStream(System.out)));
                ps.setTimestamp(1, new java.sql.Timestamp(entity.getDateTime().getTimeInMillis()));
                ps.setString(2, entity.getDesc());
                ps.setInt(3, entity.getSessionTime());
                ps.setFloat(4, entity.getDistInKm());
                ps.setInt(5, entity.getAvrgHeartRate());
                ps.setInt(6, entity.getType().getId());
                ps.setInt(7, entity.getId());
                ps.executeUpdate();
                con.commit();
                con.setAutoCommit(true);
            } catch (SQLException e) {
                con.rollback();
                con.setAutoCommit(true);
                System.err.println("Chyba při práci s databází: " + e.getMessage());
                throw new DataEntryException(e.getMessage());
            }
        } catch (SQLException e) {
            System.err.println("Chyba při komunikaci s databází: " + e.getMessage());
            throw new NoConnectionException(e.getMessage());
        }
    }

    /**
     * Vymazání daného záznamu z databáze.
     * 
     * @param RunEntity
     * @throws NoConnectionException 
     */
    @Override
    public void delete(RunEntity entity) throws NoConnectionException {

        try (Connection con = getConnection(); PreparedStatement ps = con.prepareStatement(DELETE)) {
            //DriverManager.setLogWriter(new PrintWriter(new BufferedOutputStream(System.out)));
            ps.setInt(1, entity.getId());
            ps.executeUpdate();
            this.entities.remove(entity.getId());

        } catch (SQLException e) {
            System.err.println("Chyba při komunikaci s databází: " + e.getMessage());
            throw new NoConnectionException(e.getMessage());
        }
    }
    
    @Override
    public void deleteAll() throws NoConnectionException {

        try (Connection con = getConnection(); PreparedStatement ps = con.prepareStatement(DELETEALL)) {
            //DriverManager.setLogWriter(new PrintWriter(new BufferedOutputStream(System.out)));
            ps.executeUpdate();
            this.entities.clear();

        } catch (SQLException e) {
            System.err.println("Chyba při komunikaci s databází: " + e.getMessage());
            throw new NoConnectionException(e.getMessage());
        }
    }
    
    public List<RunEntity> allJoin() throws NoConnectionException {

        List<RunEntity> arrayList = new ArrayList();

        try (Connection con = getConnection(); 
                PreparedStatement ps = con.prepareStatement(JOIN)) {
            //DriverManager.setLogWriter(new PrintWriter(new BufferedOutputStream(System.out)));
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                long time = rs.getTimestamp(2).getTime();
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(time);
                RunEntity entity = new RunEntity(rs.getInt(1), 
                        calendar, rs.getString(3), rs.getInt(4), 
                        rs.getFloat(5), rs.getInt(6), 
                        new TypeEntity(rs.getInt(8), rs.getString(9))); 
                arrayList.add(entity);
            }
            return arrayList;
        } catch (SQLException e) {
            System.err.println("Chyba při komunikaci s databází: " + e.getMessage());
            throw new NoConnectionException(e.getMessage());
        }
    }
    
    @Override
    public int getFreeId() {
        
        int i;
        
        try (Connection con = getConnection(); 
                PreparedStatement ps = con.prepareStatement(GET_ID)) {
            //DriverManager.setLogWriter(new PrintWriter(new BufferedOutputStream(System.out)));
            ResultSet rs = ps.executeQuery();
            
            for (i = 1; rs.next(); i++) {
                
                if (rs.getInt(1) != i) {
                    // return free ID
                    return i;
                }
            }
            // else return first free ID out of database
            return i;
            
        } catch (SQLException e) {
            System.err.println("Chyba při komunikaci s databází: " + e.getMessage());
            throw new NoConnectionException(e.getMessage());
        }
    }
}
