
package cz.cvut.fel.skoumond.sql;

import cz.cvut.fel.skoumond.entities.TypeEntity;
import cz.cvut.fel.skoumond.exceptions.DataEntryException;
import cz.cvut.fel.skoumond.exceptions.NoConnectionException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Třída reprezentující typy tréninků v databázi.
 * 
 * @author Ondra
 */
public class TypeRepository extends Repository<TypeEntity> {

    private static TypeRepository instance = null;
    /**
     *  Příkaz SQL pro hledání podle identifikačního čísla .
     */
    private final String FINDBYID = "SELECT * FROM APP.TYPE WHERE T_ID = ?";
    /**
     *  Příkaz SQL pro vyhledání všech tréninkových typů.
     */
    private final String ALL = "SELECT * FROM APP.TYPE";
    /**
     *  Příkaz SQL pro vložení nových tréninkových typů do databáze.
     */
    private final String INSERT = "INSERT INTO APP.TYPE VALUES (?, ?)";
    /**
     *  Příkaz SQL pro aktualizaci již uložených tréninkových typů v databázi.
     */
    private final String UPDATE = "UPDATE APP.TYPE SET T_TYPE = ? WHERE T_ID = ?";
    /**
     *  Příkaz SQL pro smazání daných tréninkových typů z databáze.
     */
    private final String DELETE = "DELETE FROM APP.TYPE WHERE T_ID = ?";
    
    private final String DELETEALL = "DELETE FROM APP.TYPE";
    
    private final String GET_ID = "SELECT T_ID FROM APP.TYPE ORDER BY T_ID";

    private TypeRepository() {
        entities = new HashMap();
    }
    
    public static TypeRepository getInstance() {
        if (instance == null) {
            instance = new TypeRepository();
        }
        return instance;
    }

    /**
     * Najde tréninkový typ pomocí identifikačního čísla.
     * 
     * @param index
     * @return TypeEntity
     * @throws NoConnectionException 
     */
    @Override
    public TypeEntity findById(int index) throws NoConnectionException {

        if (this.entities.containsKey(index)) {
            return this.entities.get(index);
        }
        
        try (Connection con = getConnection(); PreparedStatement ps = con.prepareStatement(FINDBYID)) {
            //DriverManager.setLogWriter(new PrintWriter(new BufferedOutputStream(System.out)));
            ps.setInt(1, index);
            ResultSet rs = ps.executeQuery();
            rs.next();
            return new TypeEntity(rs.getInt(1), rs.getString(2));
        } catch (SQLException e) {
            System.err.println("Chyba při komunikaci s databází: " + e.getMessage());
            throw new NoConnectionException(e.getMessage());
        }
    }

    /**
     * Vrací kolekci všech tréninkových typů v databázi.
     * 
     * @return List<RunEntity> 
     * @throws NoConnectionException 
     */
    @Override
    public List<TypeEntity> all() throws NoConnectionException {

        List<TypeEntity> arrayList = new ArrayList();

        try (Connection con = getConnection(); PreparedStatement ps = con.prepareStatement(ALL)) {
            //DriverManager.setLogWriter(new PrintWriter(new BufferedOutputStream(System.out)));
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                TypeEntity entity = new TypeEntity(rs.getInt(1), rs.getString(2));
                arrayList.add(entity);
                entities.put(entity.getId(), entity);
            }
            return arrayList;
        } catch (SQLException e) {
            System.err.println("Chyba při komunikaci s databází: " + e.getMessage());
            throw new NoConnectionException(e.getMessage());
        }
    }

    /**
     * Uloží daný tréninkový typ do databáze.
     * 
     * @param TypeEntity
     * @throws NoConnectionException 
     */
    @Override
    public void save(TypeEntity entity) throws NoConnectionException {

        try (Connection con = getConnection(); PreparedStatement ps = con.prepareStatement(INSERT)) {
            //DriverManager.setLogWriter(new PrintWriter(new BufferedOutputStream(System.out)));
            ps.setInt(1, entity.getId());
            ps.setString(2, entity.getType());
            ps.executeUpdate();
            this.entities.put(entity.getId(), entity);

        } catch (SQLException e) {
            System.err.println("Chyba při komunikaci s databází: " + e.getMessage());
            throw new NoConnectionException(e.getMessage());
        }
    }

    /**
     * Aktualizace již uloženého tréninkového typu v databázi.
     * 
     * @param TypeEntity
     * @throws NoConnectionException 
     */
    @Override
    public void update(TypeEntity entity) throws NoConnectionException, DataEntryException {

        try (Connection con = getConnection();) {

            con.setAutoCommit(false);
            try (PreparedStatement ps = con.prepareStatement(UPDATE)) {
                //DriverManager.setLogWriter(new PrintWriter(new BufferedOutputStream(System.out)));
                ps.setString(1, entity.getType());
                ps.setInt(2, entity.getId());
                ps.executeUpdate();
                con.commit();
                con.setAutoCommit(true);
            } catch (SQLException e) {
                con.rollback();
                con.setAutoCommit(true);
                System.err.println("Chyba při práci s databází: " + e.getMessage());
                throw new DataEntryException(e.getMessage());
            }
        } catch (SQLException e) {
            System.err.println("Chyba při komunikaci s databází: " + e.getMessage());
            throw new NoConnectionException(e.getMessage());
        }
    }

    /**
     * Vymazání daného tréninkového typu z databáze.
     * 
     * @param TypeEntity
     * @throws NoConnectionException 
     */
    @Override
    public void delete(TypeEntity entity) throws NoConnectionException {

        try (Connection con = getConnection(); PreparedStatement ps = con.prepareStatement(DELETE)) {
            //DriverManager.setLogWriter(new PrintWriter(new BufferedOutputStream(System.out)));
            ps.setInt(1, entity.getId());
            ps.executeUpdate();
            this.entities.remove(entity.getId());

        } catch (SQLException e) {
            //System.err.println("Chyba při komunikaci s databází: " + e.getMessage());
            throw new NoConnectionException(e.getMessage());
        }
    }
    
    @Override
    public void deleteAll() throws NoConnectionException {

        try (Connection con = getConnection(); PreparedStatement ps = con.prepareStatement(DELETEALL)) {
            //DriverManager.setLogWriter(new PrintWriter(new BufferedOutputStream(System.out)));
            ps.executeUpdate();
            this.entities.clear();

        } catch (SQLException e) {
            System.err.println("Chyba při komunikaci s databází: " + e.getMessage());
            throw new NoConnectionException(e.getMessage());
        }
    }
    
    @Override
    public int getFreeId() {
        
        int i;
        
        try (Connection con = getConnection(); 
                PreparedStatement ps = con.prepareStatement(GET_ID)) {
            //DriverManager.setLogWriter(new PrintWriter(new BufferedOutputStream(System.out)));
            ResultSet rs = ps.executeQuery();
            
            for (i = 1; rs.next(); i++) {
                
                if (rs.getInt(1) != i) {
                    // return free ID
                    return i;
                }
            }
            // else return first free ID out of database
            return i;
            
        } catch (SQLException e) {
            System.err.println("Chyba při komunikaci s databází: " + e.getMessage());
            throw new NoConnectionException(e.getMessage());
        }
    }
}
