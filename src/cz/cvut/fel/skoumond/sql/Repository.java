
package cz.cvut.fel.skoumond.sql;

import cz.cvut.fel.skoumond.entities.BaseEntity;
import cz.cvut.fel.skoumond.exceptions.NoConnectionException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Třída spravující pipojení k databázi.
 * 
 * @author Ondra
 */
public abstract class Repository<T extends BaseEntity> implements IRepository<T> {

    /**
     * Kolekce tvořící spojení ID - záznam.
     */
    protected HashMap<Integer, T> entities; 

    /**
     * Konstruktor pro vytvoření Hashmapy.
     */
    protected Repository() {
        entities = new HashMap<>();
    }
    
    /**
     * Mtoda pro vytvoření spojení s databází.
     * 
     * @return Connection
     * @throws NoConnectionException 
     */
    protected Connection getConnection() throws NoConnectionException {

        try {
            // JDBC driver
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
        } catch (java.lang.ClassNotFoundException e) {
            System.err.println("Chyba při načtení databázového ovladače: " + e.getMessage());
        }

        try {            
            return DriverManager.getConnection("jdbc:derby:database;create=true", "skoumond", "heslo");
        } catch (SQLException e) {
            System.err.println("Chyba při komunikaci s databází: " + e.getMessage());
            throw new NoConnectionException();
        }
    }
}
