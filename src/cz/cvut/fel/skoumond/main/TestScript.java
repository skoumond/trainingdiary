/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.skoumond.main;

import cz.cvut.fel.skoumond.entities.RunEntity;
import cz.cvut.fel.skoumond.entities.TypeEntity;
import cz.cvut.fel.skoumond.sql.RunRepository;
import cz.cvut.fel.skoumond.sql.TypeRepository;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author Ondra
 */
public class TestScript {
    
    public static void main(String[] args) {
      
        // ***** DATABASE TEST SCRIPT *****

        System.out.println("Welcome to Test Script!");
        
        Calendar calendar = Calendar.getInstance();

        // creating TypeEntity objects
        TypeEntity sprint = new TypeEntity(1, "Sprint running");
        TypeEntity marathon = new TypeEntity(2, "Marathon running");
        TypeEntity relax = new TypeEntity(3, "Relax running");
        TypeEntity trainingBasic = new TypeEntity(4, "Basic training");
        TypeEntity trainingAdvanced = new TypeEntity(5, "Advanced training");
        TypeEntity trainingExpert = new TypeEntity(6, "Expert training");
        System.out.println("TypeEntity objects created");
        
        // creating RunEntity objects
        RunEntity run1 = new RunEntity(7, calendar, "relaxace", 45, (float) 4.5, 119, relax);
        RunEntity run2 = new RunEntity(8, calendar, "trénink", 60, (float) 7, 125, trainingBasic);
        RunEntity run3 = new RunEntity(9, calendar, "trénink", 60, (float) 6, 140, sprint);
        RunEntity run4 = new RunEntity(10, calendar, "lehká relaxace", 45, (float) 4.5, 119, relax);
        RunEntity run5 = new RunEntity(11, calendar, "tvrdý trenink", 60, (float) 7, 140, trainingExpert);
        RunEntity run6 = new RunEntity(12, calendar, "trenink v horách", 75, (float) 6, 140, trainingExpert);
        RunEntity run7 = new RunEntity(13, calendar, "běh s Adamem", 60, (float) 4.5, 139, trainingAdvanced);
        RunEntity run8 = new RunEntity(14, calendar, "trénink na Strahově", 65, (float) 7, 125, trainingBasic);
        RunEntity run9 = new RunEntity(15, calendar, "trenink fotbalový", 70, (float) 6, 135, trainingAdvanced);
        RunEntity run10 = new RunEntity(16, calendar, "lehká relaxace", 45, (float) 4.5, 129, relax);
        RunEntity run11 = new RunEntity(17, calendar, "sprinty kopce", 60, (float) 7, 145, sprint);
        RunEntity run12 = new RunEntity(18, calendar, "dlouhý výběh", 120, (float) 6, 130, marathon);
        System.out.println("RunEntity objects created");
        
        // create Type table repository
        TypeRepository type = TypeRepository.getInstance();

        //populating Type table
        System.out.println("Populating Type table...");
        type.save(sprint);
        type.save(marathon);
        type.save(relax);
        type.save(trainingBasic);
        type.save(trainingAdvanced);
        type.save(trainingExpert);

        // create Run table repository
        RunRepository run = RunRepository.getInstance();

        // populating Run table
        System.out.println("Populating Run table...");
        run.save(run1);
        run.save(run2);
        run.save(run3);
        run.save(run4);
        run.save(run5);
        run.save(run6);
        run.save(run7);
        run.save(run8);
        run.save(run9);
        run.save(run10);
        run.save(run11);
        run.save(run12);
        
        // search trainings by ID
        System.out.println("Searching...");
        System.out.println("Searching for ID 11 in Run...");
        System.out.println(run.findById(11));
        System.out.println("Searching for ID 13 in Run...");
        System.out.println(run.findById(13));
        System.out.println("Searching for ID 16 in Run...");
        System.out.println(run.findById(16));
        
        // search by non-existing ID - causes exception
        System.out.println("Searching...");
        System.out.println("Searching for ID 5 in Run...");
        //System.out.println(run.findById(5));
        
        // updating object 
        System.out.println("Updating objects...");
        run2.setDesc("trenink s adou");
        run3.setDistInKm(10);

        // updating database
        System.out.println("Updating database...");
        run.update(run2);
        run.update(run3);
        
        // get all objects
        System.out.println("Creating ArrayList...");
        ArrayList<RunEntity> list = (ArrayList) run.all();
        System.out.println("Printing ArrayList...");
        for (RunEntity r : list) {
            System.out.println(r);
        }
        
        // deleting objects
        System.out.println("Deleting objects...");
        run.delete(run12);
        run.delete(run11);
        run.delete(run10);
        run.delete(run9);
        run.delete(run8);
        run.delete(run7);
        
        // ***** end of test script *****
        System.out.println("Konec..."); 
    }
}
