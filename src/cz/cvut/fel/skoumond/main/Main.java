package cz.cvut.fel.skoumond.main;

import cz.cvut.fel.skoumond.gui.Mainframe;

/**
 * Activity diary
 *
 * @author Ondřej Skoumal
 */
public class Main {

    /**
     * @param args the command line arguments
     *
     * Obsahuje příkaz spuštění hlavního menu.
     */
    
    public static void main(String[] args) throws Exception {
        
        // Run program
        Mainframe  mainframe = new Mainframe();
    }
}
