package cz.cvut.fel.skoumond.utilities;

import cz.cvut.fel.skoumond.entities.RunEntity;
import cz.cvut.fel.skoumond.entities.TypeEntity;
import cz.cvut.fel.skoumond.sql.RunRepository;
import cz.cvut.fel.skoumond.sql.TypeRepository;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;

/**
 *
 * @author Ondra
 */
public class FileUtils {

    private static FileUtils instance = null;

    private FileUtils() {
    }

    public static FileUtils getInstance() {
        if (instance == null) {
            instance = new FileUtils();
        }
        return instance;
    }

    /**
     * Metoda pro ulozeni databaze.
     *
     * @param fileName
     * @throws IOException IO chyba
     */
    public void saveDatabase(String fileName) throws IOException {

        DatabaseSaver dSaver = new DatabaseSaver();

        try {
            try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName))) {
                out.writeObject(dSaver);
            }
        } catch (IOException e) {
            throw e;
        }
    }

    /**
     * Metoda pro import databaze.
     *
     * @param fileName
     * @throws IOException IO chyba
     * @throws ClassNotFoundException chyba pri nenalezeni tridy
     */
    public void importDatabase(String fileName) throws IOException, ClassNotFoundException {
        DatabaseSaver dSaver;
        try {
            try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(new File(fileName)))) {
                dSaver = (DatabaseSaver) in.readObject();
                
                List<RunEntity> activities = dSaver.getdList();
                List<TypeEntity> aTypes = dSaver.getvList();

                for (TypeEntity te : aTypes) {
                    TypeRepository.getInstance().save(te);
                }

                for (RunEntity r : activities) {
                    RunRepository.getInstance().save(r);
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            throw e;
        }
    }

    /**
     * Metoda pro ulozeni souboru do textu.
     *
     * @throws IOException IO chyba
     */
    public void saveTablesToTxt() throws IOException {

        String activities = "";

        List<RunEntity> runEntities = RunRepository.getInstance().allJoin();

        for (RunEntity re : runEntities) {
            activities += re + "\n";
        }

        try (Writer out = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream("Run activities.txt"), "UTF8"))) {
            out.write(activities);
        } catch (IOException e) {
            throw e;
        }
    }
}
