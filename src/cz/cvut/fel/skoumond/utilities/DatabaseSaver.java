
package cz.cvut.fel.skoumond.utilities;

import cz.cvut.fel.skoumond.entities.RunEntity;
import cz.cvut.fel.skoumond.entities.TypeEntity;
import cz.cvut.fel.skoumond.sql.RunRepository;
import cz.cvut.fel.skoumond.sql.TypeRepository;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Ondra
 */
public class DatabaseSaver implements Serializable {

    private List<RunEntity> rEntities = RunRepository.getInstance().all();
    private List<TypeEntity> tEntities = TypeRepository.getInstance().all();

    public DatabaseSaver() {
    }

    public List<RunEntity> getdList() {
        return rEntities;
    }

    public List<TypeEntity> getvList() {
        return tEntities;
    }
}
