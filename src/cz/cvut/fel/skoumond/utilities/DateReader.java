
package cz.cvut.fel.skoumond.utilities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;


/**
 * Třída sloužící pro správné získání formátu data a času
 *
 * @author Ondra
 */

public class DateReader {
    
    /**
     * formatovani dne, mesice, roku, hodiny a minut, staticke aby bylo
     * pouzitelne pro vsechny tridy
     */
    public static DateFormat formatData = new SimpleDateFormat("d.M.yyyy H:mm");
    /**
     * formatovani data bez casu
     */
    public static DateFormat formatDataWithountTime = new SimpleDateFormat("d.M.yyyy");
    
    boolean read;

    /**
     * vytvoreni instance podporuj9c9 Gui
     */
    public DateReader() {
         read = true;
    }
    
    /**
     * Nacita fotmatovane datum, vraci instanci calendare, rekurzivne vola sebe
     * sama pri spatnem zadani
     *
     * @param Dateformat
     *
     */
    private Calendar readCalendar(DateFormat formatData, String string) {
        Calendar dateTime = Calendar.getInstance();
        try {
            dateTime.setTime(formatData.parse(string));
            // kontrola data prisnym formatovanim
            dateTime.setLenient(false);
            dateTime.getTime();
        } catch (Exception e) {
            read = false; //throw e;
        }
        return dateTime;
    }

    /**
     * Ziska spravne formatovane datum a cas
     */
    public Calendar readDateTime(String string) {
        //"Zadejte datum a čas ve tvaru [1.1.2013 14:00]
        if (!read) {
            throw new NumberFormatException();
        }
        return readCalendar(formatData, string);
    }

    /**
     * Ziska spravne formatovane datum, bez casu
     */
    public Calendar readDate(String string) {
        //System.out.println("Zadejte datum ve tvaru [1.1.2013]:");
        if (!read) {
            throw new NumberFormatException();
        }
        return readCalendar(formatDataWithountTime, string);
    }
}
