
package cz.cvut.fel.skoumond.exceptions;

/**
 *
 * @author Ondra
 */
public class NoConnectionException extends RuntimeException {
    
    public NoConnectionException() {
        super();
    }

    public NoConnectionException(String string) {
        super(string);      
    }

    public NoConnectionException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public NoConnectionException(Throwable thrwbl) {
        super(thrwbl);      
    }

    protected NoConnectionException(String string, Throwable thrwbl, boolean bln, boolean bln1) {
        super(string, thrwbl, bln, bln1);       
    }    
}
