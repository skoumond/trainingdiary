
package cz.cvut.fel.skoumond.exceptions;

/**
 *
 * @author Ondra
 */
public class DataEntryException extends RuntimeException {
    
    public DataEntryException() {
        super();
    }

    public DataEntryException(String string) {
        super(string);      
    }

    public DataEntryException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public DataEntryException(Throwable thrwbl) {
        super(thrwbl);      
    }

    protected DataEntryException(String string, Throwable thrwbl, boolean bln, boolean bln1) {
        super(string, thrwbl, bln, bln1);       
    }    
}
